#include <iostream>
#include <fstream>
#include <string>
#include <TGraph.h>
#include <TMultiGraph.h>
#include <TLegend.h>
#include <TAxis.h>
#include <TCanvas.h>

using namespace std;

void plotResults(){

  // Make arrays for data
  const int nChan = 16;
  const int nThresh = 51;
  double aveHits[nChan][nThresh];

  // Write threshold array
  double thresholds[nThresh];
  for (int i = 0; i < nThresh; i++){
    thresholds[i] = i * 5;
  }

  // Read in file
  ifstream inFile("results.txt");

  // Skip titles
  string line;
  getline(inFile,line);

  string mask;
  double threshold, num_reads, acc_time, active, total_hits, rate;

  int chan = 0;
  int meas = 0;

  while(inFile >> mask){

    // reset counters if we've filled a channel
    if(meas == nThresh){
      meas = 0;
      chan++;
    }

    inFile >> threshold >> num_reads >> acc_time >> active >> total_hits >> rate;
    aveHits[chan][meas] = total_hits/num_reads + (chan+1)*2016;

    meas++;
  }

  // Put data into TGraphs
  TGraph* tg[nChan];
  int color[nChan] = {1,kRed,kSpring-1,kBlue, kYellow, kOrange, kMagenta, kTeal, kAzure, kPink, kGreen, kViolet, kSpring-7, kViolet-6, kRed+2, kAzure+2};
  TMultiGraph* mg = new TMultiGraph();

  for (int i = 0; i < nChan; i++){
    tg[i] = new TGraph(nThresh, thresholds, aveHits[i]);
    tg[i]->SetLineColor(color[i]);
    tg[i]->SetLineWidth(3);
    mg->Add(tg[i],"l");
  }

  TCanvas* c1 = new TCanvas("c1");

  mg->SetTitle(";ASDQ Threshold / mV; Mean No. Hits (+ Channel x 2016)");
  mg->Draw("AL");
  mg->GetXaxis()->SetLimits(0,300);

  TLegend* leg = new TLegend(0.7816,0.1205,0.9583,0.8879);
  for (int i = 0; i < nChan; i++){leg->AddEntry(tg[i], Form("Channel %d",16-i)," ");}
  leg->SetFillStyle(0);
  leg->SetBorderSize(0);
  leg->Draw();

  c1->SaveAs("ThresholdScan.png");

  return;
}
