#!/usr/bin/python

import sys, time, getopt
exit

#sys.path.append('../lib') #Hack to add the local lib dir to PYTHONPATH (now in trackerDaq-setup.sh)
import PyGm2

def main(argv):

    runNumber = 0;

    #Scan over channels and thresholds
    for disc in range (200,3001,200):
        #Set start value for scan based on three test points
        startValue = 11.34 + disc*0.7157 - 0.000112054*disc*disc - 200
        if startValue < 0:
            startValue = 0
        print startValue
        
        for threshold in range (startValue,startValue+401,5):
            newThreshold = 'TDC_THRESHOLDS ' + str(threshold) + ' ' + str(threshold) + ' ' + str(disc)+ ' 2000 ' + str(threshold) + ' ' + str(threshold) + ' ' + str(disc) + ' 2000'
            print newThreshold

            infile = open('../cfg/StrawsDAQ.cards')
            outfile = open('cardFiles/cardFile_'+str(runNumber)+'.cards', 'w')
            for line in infile:
                line = line.replace('TDC_THRESHOLDS 0 0 300 2000 0 0 300 2000', newThreshold)
                outfile.write(line)
            
            infile.close()
            outfile.close()
                
            sm = PyGm2.StrawsDAQ('cardFiles/cardFile_'+str(runNumber)+'.cards')
        
            #Configure command
            sm.configure(runNumber)

            #Start command 
            sm.start()
    
            #Check how many pages we've read
            while (sm.getNumPagesReadThisSpill() < 200):
                time.sleep(0.1)
 
            #Stop command
            sm.stop()

            #Shutdown behaviour
            sm.shutDown()

            #Delete sm so that it re-creates with new card file
            del sm

            runNumber += 1

    #Exit
    sys.exit()

#Print usage information
def usage():
      print ''
      print 'Usage: ThresholdScan.py'
      print ''

if __name__ == '__main__':
    main(sys.argv[1:])

