# -- Critical errors ------------------
#
# Suppress critical errors (e.g. ones that terminate program) to allow it to continue
# Note: Not advised!
SUPPRESS_CRITICAL_ERROR 0
#
# -- Logging tools ------------------
#
# Set debug level: 0 (error only), 1 (error+warning), 2 (error+warning+info), 3(error+warning+info+debug)
LOGGER_DEBUG_LEVEL  2
LOG_TO_FILE 0
# Don't add file extension to logger file, this will be added automatically (along with date)
LOGGER_FILE /home/G2Muon/DAQ/gm2-tracker-readout-daq/software/results/TestPulseThresholdScan
#
# -- IPBus config ------------------
#
CONN_FILE file:///home/G2Muon/DAQ/gm2-tracker-readout-daq/software/cfg/connections.xml
DEVICE_ID atlys0-direct-BU
#
# -- ATLYS params ------------------
#
PAGE_SIZE 2048
SPY_PTR_SIZE 16
SPY_BUFF_ID ram
#
# -- DAQ config ------------------
#
# Define whether using external or manual triggering
EXT_TRIGGER 1
# Time (ms) software allows TDCs to accumulate for before stopping them and reading out the data
SW_ACCUMULATE_TIME 20
# Timestep (e.g. between polls on R ptr) and timeout for checking readout progress (both ms)
# - Timeout should be large enough to allow readout to complete under nominal conditions
READOUT_TIMESTEP 0
READOUT_TIMEOUT 100.0
# Period between ATLYS-generated triggers in 40GHz/25ns cycles (e.g. number of 40GHz cycles between gen_spill cmds)
#   400,000 * 25ns = 10ms (current getting HW trigg at 1Hz)
HW_TRIG_PERIOD 400000
# Number of ATLYS-generated triggers to send after each external trigger
#   600 gives 6s (e.g. 4s spill + margin) spill for if period = 10ms
NUM_HW_TRIGS 1
# Timestep of StrawsPoll polling of registers (monitoring only)
STATUS_POLLING_TIMESTEP 1000
# Flag determining if StrawsDAQ should try to get beam info from web
GET_BEAM_INFO 0
#
# -- Debug tools ------------------
#
UHAL_DEBUG_LEVEL  0
PROFILING 0
DUMP_RAW 1
DUMP_STEP 100
NUM_SPILLS_TO_PRINT 100
#
# -- ROOT file output ------------------
#
# Trigger writing to ROOT file directly from StrawsDAQ (rather than via Midas) using these params
WRITE_TO_ROOT 1
OP_ROOT_FILE RefireRate
# Choose individual elements to be written
WRITE_STRAWS 1
WRITE_SCINT 0
WRITE_MWPC 0
WRITE_BEAM 0
#
# -- TDC Config ------------------
#
# Tell software how many TDCs are being used (assumes they are numbered sequentially from 0)
NUM_TDCS              1
# Sets which of the 16 channels for each TDC we enable. Value is in hex. Below is for 2 TDCs.
TDC_CHANNEL_MASKS 0x5500 0x5500
# TDC Thresholds (in mV) per TDC. Noise hits for thresholds in range 50-120. There are 4 values per TDC: the 1st two are fixed at zero, the third is the one we set and the fourth (0x4D8 = 1240) likely fixed. Sensible default for data-taking is 150. Sensible default to get noise hits for debugging is 100.
# They are put as integers below.
TDC_THRESHOLDS 0 1000 500 2000 0 1000 500 2000
TDC_SCALAR_ENABLE     1  
TDC_BOTH_EDGE_ENABLE  1
# This is delay between TDC receiving a start_acq signal and starting to accumulate
# - LSB is 160ns
# - Needs to be non-zero currently to avoid spurious t<5ns hits from TDC
TDC_START_OFFSET 100
# TDC accumulate period is time that TDCs should accumulate hits for after receiving a start_acq signal
# LSB is 160 ns so 0xFFFE is 10.32ms; 0xFFFF is 10.48ms. Allow small gap between end of accumulation and data sending.
# Values are in HEX.  (for 10ms running : then 3 values are FFFE,FFFF,FFFE; for 3ms: 47FE, 47FF, 47FE)
TDC_ACCUMULATE_PERIOD   0x47FE
# Time (LSB=160ns) to wait after TDC accumulation complete before sending data
TDC_DATA_SEND_OFFSET    0x47FE
# Don't turn on the sel_b mode
TDC_SEL_B_TIME        0x47FE
#
# -- TDC power cycle ------------------
#
# Duration (s) TDCs stay ON for after power ON command
POWER_ON_DURATION_S 10000
# Offset (s) between t0 and first power ON command
POWER_ON_OFFSET_S -1
# Duration (s) TDCs stay OFF for after power OFF command
POWER_OFF_DURATION_S 3
# Time to wait after TDC power ON command sent to allow power up
TDC_POWER_ON_DELAY_MS 500
# Timestep (ms) for TDC power control loop
POWER_POLLING_TIMESTEP 1000
