#include <iostream>
#include <fstream>
#include <string>
#include <TGraph.h>
#include <TMultiGraph.h>
#include <TLegend.h>
#include <TAxis.h>
#include <TCanvas.h>

using namespace std;

double fitFunc(double* x, double* par){

  double thr = x[0];
  double riseStart = par[0];
  double riseWidth = par[1];
  double riseHeight = par[2];

  double rate;

  if(thr < riseStart){
    rate = 0;
  } else if (thr > riseStart && thr < riseStart + riseWidth){
    rate = (thr-riseStart)*(riseHeight/riseWidth);
  } else {
    rate = riseHeight;
  }

  return rate;
}

void plotResults(){

  // Make arrays for data
  const int nDisc = 15;
  vector<double> rate[nDisc];
  vector<double> thresholds[nDisc];

  vector<double> rate_NoNoise[nDisc];
  vector<double> thresholds_NoNoise[nDisc];

  // Vectors for final plot
  vector<double> discThr;
  vector<double> testPulseAmp;
  vector<double> discThrErr;
  vector<double> testPulseAmpErr;

  // Read in file
  ifstream inFile("results.txt");

  // Skip titles
  string line;
  getline(inFile,line);

  string mask;
  double threshold, num_reads, acc_time, active, total_hits, hitrate;

  int nThresh = 81;
  int discSetting = 0;
  discThr.push_back(200);
  discThrErr.push_back(0);
  int meas = 0;
  int stepSize = 5;
  double prev_rate = 0;
  double thresh = 0;

  while(inFile >> mask){

    if(meas == nThresh){
      discSetting++;
      meas = 0;

      // Find starting point (OK for first run through since this started at 0)
      double disc = 200 + discSetting*200;
      discThr.push_back(disc);
      discThrErr.push_back(0);
      thresh = (int)(11.34 + disc*0.7157 - 0.000112054*disc*disc - 200);
      if(thresh < 0) thresh = 0;
    }

    inFile >> threshold >> num_reads >> acc_time >> active >> total_hits >> hitrate;

    //    cout << mask << " " << threshold << " " << num_reads << " " << acc_time << " " << active << " " << total_hits << " " << hitrate << endl;

    double rate_tmp = total_hits/(num_reads*2.9488e-3);  // 2.9488 ms in accumulation
    rate_tmp /= 8.; // get rate per pulsed channel

    if(rate_tmp - prev_rate < 1000){
      rate_NoNoise[discSetting].push_back(rate_tmp);
      thresholds_NoNoise[discSetting].push_back(-3000 + 2*thresh);  // Convert from TDC DAC to ASDQ ref voltage (0-3V -> -3-3V)
      prev_rate = rate_tmp;
    }

    rate[discSetting].push_back(rate_tmp);
    thresholds[discSetting].push_back(-3000 + 2*thresh);  // Convert from TDC DAC to ASDQ ref voltage (0-3V -> -3-3V)

    thresh += stepSize;
    meas++;
  }

  // Put data into TGraphs
  TCanvas* c1 = new TCanvas("c1");

  TGraph* tg[nDisc];
  TF1* f[nDisc];
  TLegend* leg[nDisc];

  for(int i = 0; i < nDisc; i++){
    tg[i] = new TGraph(thresholds_NoNoise[i].size(), &thresholds_NoNoise[i][0], &rate_NoNoise[i][0]);
    tg[i]->SetLineWidth(3);
    tg[i]->SetTitle(";Test Pulse Control Threshold (mV); Rate per pulsed channel (Hz)");
    tg[i]->GetXaxis()->SetLimits(-3500, 0);
    tg[i]->Draw("AL");

    double midPoint = 0;
    for(int j = 0; j < thresholds_NoNoise[i].size(); j++){
      if(rate_NoNoise[i].at(j) > 1500){
	midPoint = thresholds_NoNoise[i].at(j);
	break;
      }
    }

    f[i] = new TF1("f1",fitFunc,-3000,0,3);
    f[i]->SetParameter(0,midPoint-50); // Start
    f[i]->SetParameter(1,100);      // Width
    f[i]->SetParLimits(1,30,130);   // Width
    f[i]->FixParameter(2,3031);     // Stop height
    f[i]->SetLineColor(2);
    f[i]->SetLineWidth(1);
    f[i]->SetNpx(10000);
    f[i]->Draw("SAME");

    tg[i]->Fit(f[i],"RQ");

    leg[i] = new TLegend(0.02,0.67,0.5,0.95);
    leg[i]->AddEntry(f[i],Form("Rise Start = %.1f #pm %.1f mV",f[i]->GetParameter(0),f[i]->GetParError(0))," ");
    leg[i]->AddEntry(f[i],Form("Rise Width = %.1f #pm %.1f mV",f[i]->GetParameter(1),f[i]->GetParError(1))," ");
    leg[i]->AddEntry(f[i],Form("Rise Height = %.1f mV",f[i]->GetParameter(2))," ");
    leg[i]->SetFillStyle(0);
    leg[i]->SetBorderSize(0);
    leg[i]->Draw();

    testPulseAmp.push_back(f[i]->GetParameter(0)+0.5*f[i]->GetParameter(1));
    testPulseAmpErr.push_back(sqrt(f[i]->GetParError(0)*f[i]->GetParError(0) + 0.25*f[i]->GetParError(1)*f[i]->GetParError(1)));

    c1->SaveAs(Form("TestPulseScan_%d.png",i));
  }

  TGraphErrors* tg1 = new TGraphErrors(discThr.size(),&discThr[0],&testPulseAmp[0], &discThrErr[0],&testPulseAmpErr[0]);
  tg1->SetTitle(";Discriminator Threshold (mV); Test Pulse 50% Level (mV)");
  tg1->GetXaxis()->SetLimits(0, 3100);
  tg1->GetYaxis()->SetRangeUser(-3000, -800);
  tg1->SetMarkerStyle(20);
  tg1->Draw("AP");

  c1->SaveAs("TestPulse_vs_DiscThreshold.png");

  TGraphErrors* tg2 = new TGraphErrors(discThr.size(),&testPulseAmp[0], &discThr[0],&testPulseAmpErr[0],&discThrErr[0]);
  tg2->SetTitle(";Test Pulse 50% Level (mV);Discriminator Threshold (mV)");
  tg2->GetXaxis()->SetLimits(-3000, 3000);
  tg2->GetYaxis()->SetRangeUser(0, 3100);
  tg2->SetMarkerStyle(20);
  tg2->SetMarkerSize(0.5);
  tg2->Draw("AP");

  c1->SaveAs("DiscThreshold_vs_TestPulse.png");
  c1->SaveAs("DiscThreshold_vs_TestPulse.pdf");
  


  return;
}
