void plotHistograms(){

  // Start by making plots from long 200ns run
  TFile* f = new TFile("Data/PulseSeparation_200ns.root");
  TTree* tree = (TTree*)f->Get("RefireRate-STRAWS/StrawsTDCProcessed");
  TBranch* branch = tree->GetBranch("StrawsTDCProcessed");

  // Set branch addresses
  int num_tdcs;
  int num_hits;
  int readout_start_time_utc_s;
  long tdc_id;
  int spill_num;
  int integration_num;
  long integration_start_time;
  long integration_end_time;
  int header_id[6];
  int header_version[1];
  int reg0_data_valid;
  int reg1_scalars_edges;
  int reg2_start_time;
  int reg3_selb_time;
  int reg4_not_used;
  int reg5_end_time;
  int reg6_not_used;
  int reg7_not_used;
  int scalar_count[16];
  double hit_time_ns[2016];
  int hit_channel[2016];
  int hit_edge[2016];

  TObjArray* leaves = branch->GetListOfLeaves();
  void* addresses[22] = {
    &num_tdcs,
    &num_hits,
    &readout_start_time_utc_s,
    &tdc_id,
    &spill_num,
    &integration_num,
    &integration_start_time,
    &integration_end_time,
    &header_id[0],
    &header_version[0],
    &reg0_data_valid,
    &reg1_scalars_edges,
    &reg2_start_time,
    &reg3_selb_time,
    &reg4_not_used,
    &reg5_end_time,
    &reg6_not_used,
    &reg7_not_used,
    &scalar_count[0],
    &hit_time_ns[0],
    &hit_channel[0],
    &hit_edge[0]
  };
  TLeaf *leaf[22];
  for(int i=0 ; i<22; i++){
    leaf[i] = (TLeaf*)leaves->At(i);
    leaf[i]->SetAddress(addresses[i]);
  }

  // Declare histograms
  TH1* hHitTime[4];
  TH1* hHitSep[4];
  TH1* hHitWidth[4];
  for(int i = 0; i < 4; i++){
    hHitTime[i]  = new TH1D(Form("hHitTime_%d",i),"Hit Time",112, 20000, 20448);
    hHitSep[i]   = new TH1D(Form("hHitSep_%d",i),"Hit Separation",10, 196.75, 203);
    hHitWidth[i] = new TH1D(Form("hHitWidth_%d",i),"Hit Width",32, 0, 20);
  }


  for(int i = 0; i < tree->GetEntries(); i++){
    tree->GetEntry(i);

    // Skip accumulations with no hits (Atlys board triggers twice for every ext trigger)
    if(num_hits == 0) continue;

    // Only take accumulations where both edges have been registered 
    if(num_hits%2 != 0) continue;

    // Kill accumulations where there's lots of noise hits
    if(num_hits > 100) continue;

    // Remove accumulations where there are hits in spurious channels
    bool skip = false;
    for(int n = 0; n < num_hits; n++){
      if(hit_channel[n] < 8 || hit_channel[n]%2 !=0) skip = true;
    }
    if(skip) continue;

    // Make and fill vectors for each channel
    vector<double> hit_time[4];

    for(int n = 0; n < num_hits; n++){
      if(hit_channel[n] == 8) hit_time[0].push_back(hit_time_ns[n]);
      if(hit_channel[n] == 10) hit_time[1].push_back(hit_time_ns[n]);
      if(hit_channel[n] == 12) hit_time[2].push_back(hit_time_ns[n]);
      if(hit_channel[n] == 14) hit_time[3].push_back(hit_time_ns[n]);
    }

    // Fill histograms with good events
    for(int j = 0; j < 4; j++){
      for(int k = 0; k < hit_time[j].size(); k++){
	if(k%2 == 0) hHitTime[j]->Fill(hit_time[j].at(k));
	if(k > 0 && k%2 == 0) hHitSep[j]->Fill(hit_time[j].at(k)-hit_time[j].at(k-2));
        if(k%2 != 0) hHitWidth[j]->Fill(hit_time[j].at(k)-hit_time[j].at(k-1));
      }
    }
  }

  gStyle->SetOptStat(0);

  TCanvas* cHitTime = new TCanvas("cHitTime");
  double maxHitTime = 0;
  for(int i = 0; i < 4; i++){
    if(hHitTime[i]->GetMaximum() > maxHitTime) maxHitTime = hHitTime[i]->GetMaximum();
  }
  hHitTime[0]->SetMaximum(1.1*maxHitTime);
  for(int i = 0; i < 4; i++){
    if(i==2){hHitTime[i]->SetLineColor(kSpring-1);}
    else{hHitTime[i]->SetLineColor(i+1);}
    hHitTime[i]->SetLineWidth(2);
    hHitTime[i]->SetTitle(";Hit Time (ns);No. Hits / 4 ns");
    if(i>0){hHitTime[i]->Draw("SAME");}
    else{hHitTime[i]->Draw();}
  }
  TLegend* leg = new TLegend(0.764,0.615,0.966,0.942);
  leg->AddEntry(hHitTime[0],"Channel 8","l");
  leg->AddEntry(hHitTime[1],"Channel 10","l");
  leg->AddEntry(hHitTime[2],"Channel 12","l");
  leg->AddEntry(hHitTime[3],"Channel 14","l");
  leg->SetFillStyle(0);
  leg->SetBorderSize(0);
  leg->Draw();
  cHitTime->SaveAs("Hit_Times_200ns.png");

  TCanvas* cHitSep = new TCanvas("cHitSep");
  double maxHitSep = 0;
  for(int i = 0; i < 4; i++){
    if(hHitSep[i]->GetMaximum() > maxHitSep) maxHitSep = hHitSep[i]->GetMaximum();
  }
  hHitSep[0]->SetMaximum(1.1*maxHitSep);
  for(int i = 0; i < 4; i++){
    if(i==2){hHitSep[i]->SetLineColor(kSpring-1);}
    else{hHitSep[i]->SetLineColor(i+1);}
    hHitSep[i]->SetLineWidth(2);
    hHitSep[i]->SetTitle(";Hit Separation (ns);No. Hits / 0.625 ns");
    if(i>0){hHitSep[i]->Draw("SAME");}
    else{hHitSep[i]->Draw();}
  }
  leg->Draw();
  cHitSep->SaveAs("Hit_Separation_200ns.png");

  TCanvas* cHitWidth = new TCanvas("cHitWidth");
  hHitWidth[0]->Draw();
  double maxHitWidth = 0;
  for(int i = 0; i < 4; i++){
    if(hHitWidth[i]->GetMaximum() > maxHitWidth) maxHitWidth = hHitWidth[i]->GetMaximum();
  }
  hHitWidth[0]->SetMaximum(1.1*maxHitWidth);
  for(int i = 0; i < 4; i++){
    if(i==2){hHitWidth[i]->SetLineColor(kSpring-1);}
    else{hHitWidth[i]->SetLineColor(i+1);}
    hHitWidth[i]->SetLineWidth(2);
    hHitWidth[i]->SetTitle(";Hit Width (ns);No. Hits / 0.625 ns");
    if(i>0){hHitWidth[i]->Draw("SAME");}
    else{hHitWidth[i]->Draw();}
  }
  leg->Draw();
  cHitWidth->SaveAs("Hit_Width_200ns.png");

  return;
}
